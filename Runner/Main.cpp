#include <stdio.h>
#include <Windows.h>
#include <string.h>

#include <String>
#include <algorithm>

char* getopt(char ** begin, char ** end, const std::string & option)
{
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool isopt(char** begin, char** end, const std::string& option)
{
	return std::find(begin, end, option) != end;
}

bool Inject(DWORD pId, char *dllName)
{
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, false, pId);
	if (hProcess)
	{
		LPVOID libAddr = (LPVOID)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
		LPVOID pathAddr = VirtualAllocEx(hProcess, NULL, strlen(dllName), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		WriteProcessMemory(hProcess, pathAddr, dllName, strlen(dllName), NULL);
		HANDLE hThread = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)libAddr, pathAddr, 0, NULL);
		WaitForSingleObject(hThread, INFINITE);
		VirtualFreeEx(hProcess, pathAddr, strlen(dllName), MEM_RELEASE);
		CloseHandle(hThread);
		CloseHandle(hProcess);
		return true;
	}
	return false;
}


int main(int argc, char **argv)
{
	int InjectDelay=0;
	char *WorkingDir=0;
	PROCESS_INFORMATION pi;
	STARTUPINFO si = { 0 };
	si.cb = sizeof(STARTUPINFO);
	if (argc < 2)
		return 0;
	char FileName[256];
	GetModuleFileName(GetModuleHandle(0), FileName, 256);
	*strrchr(FileName, '\\') = 0;
	
	//option parsing
	WorkingDir = getopt(argv, argv + argc, "-w");
	char *sDelay = getopt(argv, argv + argc, "-d");
	if (sDelay)
		InjectDelay = atoi(sDelay);

	if (WorkingDir)
		CreateProcess(NULL, argv[1], 0, 0, TRUE, 0, 0, WorkingDir, &si, &pi);
	else
		CreateProcess(NULL, argv[1], 0, 0, TRUE, 0, 0, FileName, &si, &pi);

	Sleep(InjectDelay * 1000);

	Inject(pi.dwProcessId, "Debugger.dll");
	WaitForSingleObject(pi.hProcess, INFINITE);
	return 0;
}
