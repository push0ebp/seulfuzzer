import os, sys, subprocess
import time
import shutil
from ctypes import *
from optparse import OptionParser
import threading

class Fuzzer():
    TargetFile = ''
    InputFile = ''
    WorkingDir = ''
    CrashesDir = 'Crashes'
    CrashReportDir = 'CrashReport'
    CrashDir = ''
    MutatedDir = 'Mutated'
    MutatorFileName = 'Mutator.exe' #'radamsa-0.3.exe'

    StartTime = 0
    FuzzCount = 0 
    KillTimeout = 0
    InjectDelay = 0
    
    def __init__(self, opt, args):
        self.TargetFile = args[0]
        self.TargetFileName = os.path.basename(self.TargetFile)
        self.TargetDir = os.path.dirname(self.TargetFile)

        self.InputFile = args[1]
        if self.InputFile == self.InputFile:
            self.WorkingDir = os.path.abspath('.') #Current Dir
        else:
            self.WorkingDir = opt.WorkingDir
        self.CrashesDir = self.CrashesDir + '\\' +  self.TargetFileName
        self.StartTime = time.time()

        self.InjectDelay = float(opt.InjectDelay)
        self.KillTimeout = float(opt.KillTimeout)
        
    def PrintInfo(self):
        print '[Info]'
        print 'Target : ' + self.TargetFile 
        print 'Input File : ' + self.InputFile
        print '\n[Options]'
        print 'KillTimeout : %d sec' % self.KillTimeout
        print 'InjectDelay : %d sec' % self.InjectDelay
        print 'WorkingDir : %s' % self.WorkingDir

    def Run(self, TargetFile, InputFile, WorkingDir): #Run Target
        Command = 'Runner.exe "' + os.path.abspath(self.TargetFile) + ' ' + os.path.abspath(InputFile) + '"'
        if self.InjectDelay:
            Command += ' -d ' + str(self.InjectDelay)
        if WorkingDir != '':
            Command += ' -w "' + WorkingDir + '"'
        return subprocess.call(Command)

    def Mutate(self, InputFile, OutputFile ):
        if not os.path.exists(InputFile):
            print '[!] Error : Mutation aborted, not exists inputfile'
            exit()
        subprocess.call(self.MutatorFileName + ' -o "' + OutputFile + '" "' + InputFile + '"')

    def CreateFile(self, InputFile):
        print '[+] Mutating File'
        os.path.exists(self.MutatedDir) and shutil.rmtree(self.MutatedDir)  #Remove Mutated Directory
        os.mkdir(self.MutatedDir) #Create Mutated Directory
        if os.path.isdir(InputFile): #Directory
            files = os.listdir(InputFile)
            if not files: 
                print '[!] Error : Not exists inputfile, input files to directory'
                exit()
            
            InputFile += '\\' + files[self.FuzzCount % len(files)]
        l = os.path.splitext(InputFile)
        CreatedFile = self.MutatedDir + '\\' + os.path.basename(l[0]) + '_' + str(self.FuzzCount) + l[1] #fileame_fuzzcount.ext
        self.Mutate(InputFile, CreatedFile) #Mutation
        print '    MutatedFile : ' + os.path.abspath(CreatedFile)
        return CreatedFile

    def isCrash(self):
        return os.path.exists(self.CrashReportDir) and len(os.listdir(self.CrashReportDir))

    def CrashReport(self, MutatedFile):
        files = os.listdir(self.CrashReportDir)
        for file in files: 
            not os.path.getsize(self.CrashReportDir + '\\' + file) and os.remove(self.CrashReportDir + file) #Remove empty file

        l = files[0].split('_')
        CrashDir = self.CrashesDir + '\\' + time.strftime('%Y-%m-%d_%H-%M-%S') + '_' + l[-3] + '_' + l[-2].rstrip()
        
        not os.path.exists(os.path.dirname(CrashDir)) and os.makedirs(os.path.dirname(CrashDir))
        os.path.exists(CrashDir) and shutil.rmtree(CrashDir) #Clear
        os.rename(self.CrashReportDir, CrashDir) #Move CrashReport Directory
        return CrashDir

    def Killer(self): #Kill Process
        Time = time.time()
        while not(time.time() - Time > self.KillTimeout or os.path.exists(self.CrashReportDir)): pass
        os.popen("taskkill /f /im WerFault.exe")
        os.popen("taskkill /f /im " + self.TargetFileName)

    def Fuzz(self):
        while 1:
            print '\n[*] Fuzzing...'
            print '[+] Info'
            print '    Working Time : %s, Count : %d' % (time.strftime('%H:%M:%S', time.gmtime(time.time() - self.StartTime)), self.FuzzCount)
           
            os.path.exists(self.CrashReportDir) and shutil.rmtree(self.CrashReportDir) #Remove CrashReport Directory
            MutatedFile = self.CreateFile(self.InputFile) #Create MutatedFile
            Killer = threading.Thread(target=self.Killer)
            Killer.setDaemon(0)
            Killer.start() #Start Killer Thread
            print '[+] Running...'
            self.Run(self.TargetFile, os.path.abspath(MutatedFile), self.WorkingDir)
            print '[+] Process End'
            if self.isCrash(): #Crash Event
                print '     [*] Crash !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
                SavedFile = self.CrashReport(MutatedFile) + '\\' + os.path.basename(MutatedFile)
                l = SavedFile.split('\\')
                print '         Saved file to "%s"' % l[-2]
                shutil.move(MutatedFile, SavedFile) #Move mutatedfile and Crash report
            else:
                print '     [-] Not Crash ...........'

            self.FuzzCount += 1

if __name__ == '__main__':
    parser = OptionParser("python %prog <target> <input (file or directory)>")
    parser.add_option("-t", "--timeout", dest="KillTimeout", help="Process kill timeout", default=3)
    parser.add_option("-d", "--delay", dest="InjectDelay", help="Inject delay for Themida", default=0)
    parser.add_option("-w", "--workdir", dest="WorkingDir", help="Change working directory", default=os.path.abspath('.'))
    (opt, args) = parser.parse_args()

    if len(parser.largs) < 2:
        parser.print_help()
        sys.exit(2)

    Fuzzer = Fuzzer(opt, parser.largs)   
    Fuzzer.PrintInfo()
    Fuzzer.Fuzz()