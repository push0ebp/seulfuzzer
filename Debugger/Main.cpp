#include <stdio.h>
#include <windows.h>

#include "CrashReport.h"
cCrashReport CrashReport;


struct sCode
{
	DWORD Base, Size;
}Code;

void GetCodeInfo(DWORD ImageBase, sCode *ret)
{
	IMAGE_DOS_HEADER* DosHeader = (IMAGE_DOS_HEADER*)ImageBase;
	IMAGE_NT_HEADERS *NTHeader;
	NTHeader = (IMAGE_NT_HEADERS *)((DWORD)DosHeader + DosHeader->e_lfanew);
	IMAGE_SECTION_HEADER *text = IMAGE_FIRST_SECTION(NTHeader);
	for (; strcmp((char*)text->Name, ".text"); text = (IMAGE_SECTION_HEADER*)((DWORD)text + sizeof(IMAGE_SECTION_HEADER))); //.text 섹션 찾기
	ZeroMemory(ret, sizeof(ret));
	ret->Base = ImageBase + text->VirtualAddress; 
	ret->Size = text->SizeOfRawData; 
};


LONG WINAPI ExceptionFilter(PEXCEPTION_POINTERS ExceptionInfo) {
	DWORD ExceptionAddr = (DWORD)ExceptionInfo->ExceptionRecord->ExceptionAddress;
	DWORD ExceptionCode = ExceptionInfo->ExceptionRecord->ExceptionCode;
	if ((Code.Base <= ExceptionAddr && ExceptionAddr <= Code.Base + Code.Size) &&
		ExceptionCode == STATUS_ACCESS_VIOLATION)
		return EXCEPTION_CONTINUE_SEARCH;
	
	if (!strcmp(CrashReport.GetExceptionType(ExceptionCode), "UNKNOWN_ERROR")
		|| (ExceptionCode == 0xC0000096 && *(BYTE*)ExceptionAddr == 0xFB)) //sti PRIV_INSTRUCTION for Themida
		return EXCEPTION_CONTINUE_SEARCH;


	CrashReport.CrashReport(ExceptionInfo); //Crash Report
	fclose(stdout);
	if (CrashReport.EIPChanged)
		ExitProcess(0);
	return EXCEPTION_CONTINUE_SEARCH;
}

void Hook()
{
	DWORD OldProtect;
	DWORD *Addr = (DWORD*)GetProcAddress(GetModuleHandle("kernel32.dll"), "AddVectoredExceptionHandler");
	VirtualProtect(Addr, 5, PAGE_EXECUTE_READWRITE, &OldProtect);
	*Addr = 0x08C2C031;
	*(Addr + 1) = 0x90909000;
	VirtualProtect(Addr, 5, OldProtect, &OldProtect);


	Addr = (DWORD*)GetProcAddress(GetModuleHandle("kernel32.dll"), "SetUnhandledExceptionFilter");
	VirtualProtect(Addr, 5, PAGE_EXECUTE_READWRITE, &OldProtect);
	*Addr = 0x04C2C031;
	*(Addr + 1) = 0x90909000;
	VirtualProtect(Addr, 5, OldProtect, &OldProtect);
}
	
void fMain(HMODULE hModule)
{
	GetCodeInfo((DWORD)hModule, &Code);
//	if (!access(CrashDir, 0))
//	{
		//RemoveDirectory(CrashDir);
		//CrashReport.DeleteDirectory(CrashDir);
//	}	
	AddVectoredExceptionHandler(rand() % 0xFFFFFF, ExceptionFilter);
	Hook();
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		//if (AllocConsole()) {
			//freopen("CONIN$", "rb", stdin);
			freopen("CONOUT$", "wb", stdout);
			//freopen("CONOUT$", "wb", stderr);
		//}
		fMain(hModule);
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

/*
#include <DbgHelp.h>


//Unhandled Exception 발생시 호출 될 CallBack 함수 원형
typedef BOOL(WINAPI *MINIDUMPWRITEDUMP)
(
HANDLE	hProcess,
DWORD	dwPid,
HANDLE	hFile,
MINIDUMP_TYPE DumpType,
CONST PMINIDUMP_EXCEPTION_INFORMATION	ExceptionParam,
CONST PMINIDUMP_USER_STREAM_INFORMATION	UserStreamParam,
CONST PMINIDUMP_CALLBACK_INFORMATION	CallbackParam
);

HMODULE DllHandle = LoadLibrary("DBGHELP.DLL");
if (DllHandle == NULL)
return EXCEPTION_CONTINUE_SEARCH;

//DBGHELP.DLL에서 MiniDumpWriteDump를 불러와 Dump라고 정의하며 이걸로 덤프 파일을 생성합니다.
MINIDUMPWRITEDUMP Dump = (MINIDUMPWRITEDUMP)GetProcAddress(DllHandle, "MiniDumpWriteDump");
if (Dump == NULL)
return EXCEPTION_CONTINUE_SEARCH;



SYSTEMTIME SystemTime;
GetLocalTime(&SystemTime);	//현재시간 획득

char DumpPath[256] = { 0, };
//덤프 파일 이름 설정
sprintf(DumpPath, "%d-%d-%d %d.%d.%d.dmp",
	SystemTime.wYear,
	SystemTime.wMonth,
	SystemTime.wDay,
	SystemTime.wHour,
	SystemTime.wMinute,
	SystemTime.wSecond);
//덤프 파일 생성
HANDLE FileHandle = CreateFile(DumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
if (FileHandle == INVALID_HANDLE_VALUE)
return EXCEPTION_CONTINUE_SEARCH;
//MiniDump 예외 정보 저장 구조체
_MINIDUMP_EXCEPTION_INFORMATION MiniDumpExceptionInfo;
MiniDumpExceptionInfo.ThreadId = GetCurrentThreadId();
MiniDumpExceptionInfo.ExceptionPointers = (PEXCEPTION_POINTERS)ExceptionInfo;
MiniDumpExceptionInfo.ClientPointers = NULL;

//현재 프로세스에 대한 덤프 기록을 실행합니다.
BOOL Success = Dump(
	GetCurrentProcess(),
	GetCurrentProcessId(),
	FileHandle,				//덤프를 기록할 파일 핸들
	MiniDumpNormal,
	&MiniDumpExceptionInfo,	//MiniDump 예외 정보
	NULL,
	NULL);

//덤프 기록 설공시 수행
if (Success)
{
	CloseHandle(FileHandle);
	return EXCEPTION_EXECUTE_HANDLER;
}
CloseHandle(FileHandle);
*/
