#ifndef _CRASHREPORT_H_
#define _CRASHREPORT_H_

#include <stdio.h>
#include <Windows.h>
#pragma comment(lib, "distorm.lib")
#include "distorm.h"

class cCrashReport
{
public:
	FILE *f;
	int ExceptionFlag;
	char *CrashDir = "CrashReport";
	int EIPChanged = 0;
	_DecodeResult DisAsm(DWORD Addr, int len);
	void PrintReg(PCONTEXT c);
	void StackDump(DWORD Addr, int Len);
	void CrashInfo(PEXCEPTION_POINTERS ExceptionInfo);

	void CrashReport(PEXCEPTION_POINTERS ExceptionInfo);
	
	void SaveFile(DWORD ExceptionAddr);


	char *GetTime(int Type)
	{
		SYSTEMTIME SystemTime;
		GetLocalTime(&SystemTime);
		char *Time = new char[256];
		if (Type) //File
			sprintf(Time, "%4d-%02d-%02d %02d-%02d-%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay, SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond);
		else
			sprintf(Time, "%4d-%02d-%02d %02d:%02d:%02d", SystemTime.wYear, SystemTime.wMonth, SystemTime.wDay, SystemTime.wHour, SystemTime.wMinute, SystemTime.wSecond);

		return Time;
	}
	bool DeleteDirectory(char *directory)
	{
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind;
		char path[256] = { 0 };
		strcat(path, directory);
		strcat(path, "\\*");

		hFind = FindFirstFile(path, &FindFileData);
		do{
			if (strcmp(FindFileData.cFileName, ".") != 0 && strcmp(FindFileData.cFileName, "..") != 0){
				char str[MAX_PATH];
				strcpy(str, path);
				str[strlen(path) - 1] = '\0';
				strcat(str, FindFileData.cFileName);
				if (GetFileAttributes(str) &FILE_ATTRIBUTE_DIRECTORY)
				{
					DeleteDirectory(str);
					str[strlen(path) - 1] = '\0';
					RemoveDirectory(str);
				}
				else
				{
					remove(str);
				}
			}
		} while (FindNextFile(hFind, &FindFileData));

		return true;
	}

	char* GetProcessName()
	{
		char *FileName = new char[256];
		GetModuleFileName(NULL, FileName, 256);
		return strrchr(FileName, '\\') + 1;
	}

	char* GetExceptionType(DWORD ExceptionCode)
	{
		#define EXCEPTION(x) case EXCEPTION_##x: return (#x);

		switch (ExceptionCode)
		{
			EXCEPTION(ACCESS_VIOLATION)
			EXCEPTION(DATATYPE_MISALIGNMENT)
			EXCEPTION(BREAKPOINT)
			EXCEPTION(SINGLE_STEP)
			EXCEPTION(ARRAY_BOUNDS_EXCEEDED)
			EXCEPTION(FLT_DENORMAL_OPERAND)
			EXCEPTION(FLT_DIVIDE_BY_ZERO)
			EXCEPTION(FLT_INEXACT_RESULT)
			EXCEPTION(FLT_INVALID_OPERATION)
			EXCEPTION(FLT_OVERFLOW)
			EXCEPTION(FLT_STACK_CHECK)
			EXCEPTION(FLT_UNDERFLOW)
			EXCEPTION(INT_DIVIDE_BY_ZERO)
			EXCEPTION(INT_OVERFLOW)
			EXCEPTION(PRIV_INSTRUCTION)
			EXCEPTION(IN_PAGE_ERROR)
			EXCEPTION(ILLEGAL_INSTRUCTION)
			EXCEPTION(NONCONTINUABLE_EXCEPTION)
			EXCEPTION(STACK_OVERFLOW)
			EXCEPTION(INVALID_DISPOSITION)
			EXCEPTION(GUARD_PAGE)
			EXCEPTION(INVALID_HANDLE)

		default:
			return "UNKNOWN_ERROR";
		}
		#undef EXCEPTION
	}

	void int2char(int dw)
	{
		putchar('"');
		for (int i = 0; i < 4; i++)
		{
			unsigned char c = ((char*)&dw)[i];
			if (isprint(c))
				printf("%c", c);
			else
				printf(".");
		}
		putchar('"');
	}

	char *replace(char *s, const char *olds, const char *news) {
		char *result, *sr;
		size_t i, count = 0;
		size_t oldlen = strlen(olds); if (oldlen < 1) return s;
		size_t newlen = strlen(news);


		if (newlen != oldlen) {
			for (i = 0; s[i] != '\0';) {
				if (memcmp(&s[i], olds, oldlen) == 0) count++, i += oldlen;
				else i++;
			}
		}
		else i = strlen(s);


		result = (char *)malloc(i + 1 + count * (newlen - oldlen));
		if (result == NULL) return NULL;


		sr = result;
		while (*s) {
			if (memcmp(s, olds, oldlen) == 0) {
				memcpy(sr, news, newlen);
				sr += newlen;
				s += oldlen;
			}
			else *sr++ = *s++;
		}
		*sr = '\0';

		return result;
	}

};
extern cCrashReport CrashReport;

#endif