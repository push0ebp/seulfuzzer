#include "CrashReport.h"
#include "dumpcode.h"
#include <string.h>
#include <io.h>

_DecodeResult cCrashReport::DisAsm(DWORD Addr, int len)
{
	_DecodeResult Result = (_DecodeResult)0;
	_DecodedInst *Inst = new _DecodedInst[len];
	unsigned int InstCnt = 0, i = 0;
	__try
	{
		if (Addr == 0) //goto except
		_asm {
			xor eax, eax
			mov[eax], 0
		}
		ZeroMemory(Inst, sizeof(Inst));
		Result = distorm_decode(Addr, (const unsigned char*)Addr, len * 8, Decode32Bits, Inst, len, &InstCnt);
		for (int i = 0; i < InstCnt; i++) {
			char *operands = (char*)Inst[i].operands.p;
			operands = replace(operands, "DWORD ", "");
			operands = replace(operands, "0x", "");
			printf("%08I64X (%02d) %-16s %s%s%s\r\n", Inst[i].offset, 
													  Inst[i].size, 
													  AnsiUpper((char*)Inst[i].instructionHex.p), 
													  AnsiLower((char*)Inst[i].mnemonic.p), 
													  Inst[i].operands.length != 0 ? " " : "", 
													  AnsiLower(operands));
		}
	}
	__except (true)
	{
		printf("%08X ", Addr);
		int2char(Addr);
		printf(" ? ? EIP Changed\n", Addr);
		EIPChanged = 1;
	}
	delete Inst;
	return Result;
}

void cCrashReport::PrintReg(PCONTEXT Context) {
	printf("EAX : %08X ", Context->Eax); int2char(Context->Eax); puts("");
	printf("EBX : %08X ", Context->Ebx); int2char(Context->Ebx); puts("");
	printf("ECX : %08X ", Context->Ecx); int2char(Context->Ecx); puts("");
	printf("EDX : %08X ", Context->Edx); int2char(Context->Edx); puts("");
	printf("ESI : %08X ", Context->Esi); int2char(Context->Esi); puts("");
	printf("EDI : %08X ", Context->Edi); int2char(Context->Edi); puts("\n"); 
	printf("EBP : %08X ", Context->Ebp); int2char(Context->Ebp); puts(""); 
	printf("ESP : %08X ", Context->Esp); int2char(Context->Esp); puts("\n"); 
	printf("EIP : %08X ", Context->Eip); int2char(Context->Eip); puts("");	
}
	
void cCrashReport::StackDump(DWORD Addr, int Len)
{
	dumpcode((unsigned char*)Addr, Len);
	dumpcode16((unsigned int*)Addr, Len);
}

void cCrashReport::CrashInfo(PEXCEPTION_POINTERS ExceptionInfo)
{
	DWORD ExceptionAddr = (DWORD)ExceptionInfo->ExceptionRecord->ExceptionAddress;
	DWORD ExceptionCode = ExceptionInfo->ExceptionRecord->ExceptionCode;


	printf("Target            : %s (0x%08X)\n", GetProcessName(), GetModuleHandle(0));
	printf("Crash Time        : %s\n", GetTime(0));
	printf("Command Line      : %s\n", GetCommandLine());
	printf("Exception Address : 0x%08X ", ExceptionAddr);
	int2char(ExceptionAddr);
	puts("");
	printf("Exception Code    : 0x%08X (%s)\n", ExceptionCode, GetExceptionType(ExceptionCode));
	
}


void cCrashReport::SaveFile(DWORD ExceptionAddr)
{
	_DecodedInst Inst = { 0 };
	unsigned int InstCnt = 0;
	char *ProcessName = GetProcessName();
	char FileName[256] = { 0 };

	__try
	{
		if (access(CrashDir, 0))
			CreateDirectory(CrashDir, 0);
		if (ExceptionAddr == 0) //goto except
		_asm { 
			xor eax, eax
			mov [eax], 0
		}
		distorm_decode(0, (const unsigned char*)ExceptionAddr, 10, Decode32Bits, &Inst, 1, &InstCnt);
		char *operands = (char*)Inst.operands.p;
		operands = replace(operands, "DWORD ", "");
		operands = replace(operands, "0x", "");

		sprintf(FileName, "CrashReport\\%s_0x%08X_%s %s_%s.txt", ProcessName, ExceptionAddr, AnsiLower((char*)Inst.mnemonic.p), AnsiLower((char*)operands), GetTime(1));
		strncpy(FileName, replace(FileName, ":", "."),256); //Segment Reg replace
		freopen(FileName, "w", stdout);
	}
	__except (true)
	{
		EIPChanged = 1;
		sprintf(FileName, "CrashReport\\%s_0x%08X_EIP Changed_%s.txt", ProcessName, ExceptionAddr, GetTime(1));
		strncpy(FileName, replace(FileName, ":", "."), 256); //Segment Reg replace
		freopen(FileName, "w", stdout);
	}
}
void cCrashReport::CrashReport(PEXCEPTION_POINTERS ExceptionInfo)
{
	__try
	{
		PCONTEXT Context = ExceptionInfo->ContextRecord;
		DWORD ExceptionAddr = (DWORD)ExceptionInfo->ExceptionRecord->ExceptionAddress;
		SaveFile(ExceptionAddr);
		printf("------------------------------ CRASH INFO ----------------------------------\n");
		CrashInfo(ExceptionInfo);
		printf("\n------------------------------ REGISTERS -----------------------------------\n");
		PrintReg(Context);
		printf("\n------------------------------ INSTRUCTIONS --------------------------------\n");
		DisAsm(ExceptionAddr, 20);
		printf("\n------------------------------ STACK DUMP -----------------------------------\n");
		StackDump(Context->Esp, 128);
	}
	__except (true) {  }
}
