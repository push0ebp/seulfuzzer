#ifndef _DUMPCODE_H_
#define _DUMPCODE_H_

#include <stdio.h>
#include <ctype.h>

void printchar(unsigned char c)
{
        if(isprint(c))
                printf("%c",c);
        else
                printf(".");
}

void dumpcode(unsigned char *buff, int len)
{
        int i;
        for(i=0;i<len;i++)
        {
                if(i%16==0)
					printf("%08X", &buff[i]);
				if (i % 4 == 0)
					putchar(' ');
                printf("%02X ",buff[i]);
                if(i%16-15==0)
                {
                        int j;
                       // printf("  ");
                        for(j=i-15;j<=i;j++)
                                printchar(buff[j]);
                        printf("\n");
                }
        }
        if(i%16!=0)
        {
                int j;
                int spaces=(len-i+16-i%16)*3+2;
                for(j=0;j<spaces;j++)
                        printf(" ");
                for(j=i-i%16;j<len;j++)
                        printchar(buff[j]);
        }
        printf("\n");
}


void dumpcode4(unsigned int *buff, int cnt)
{
	int i;
	for (i = 0; i < cnt; i++)
	{
		printf("%08X %08X ", &buff[i], buff[i]);
		for (int j=0; j <4; j++)
			printchar(((unsigned char*)buff)[i*4+j]);
		printf("\n");
	}
	printf("\n");
}


void dumpcode16(unsigned int *buff, int cnt)
{
	int i;
	for (i = 0; i < cnt / 4; i += 4)
	{
		printf("%08X ", &buff[i]);
		for (int j = 0; j < 4; j++)
			printf("%08X ", buff[i+j]);
		for (int j = 0; j < 16; j++)
			printchar(((unsigned char*)buff)[i*4 + j]);
		printf("\n");
	}

	printf("\n");
}

#endif